export const Config = {
  API_URL: 'https://jsonplaceholder.typicode.com/users/',
  WEATHER_SERVICE: {
    API_URL: 'http://api.openweathermap.org/data/2.5/weather',
    API_KEY: '7ff0d5c3d45ddf193e95c9038cf7b576',
    TEMPERATURE_UNIT: 'metric',
  },
}
