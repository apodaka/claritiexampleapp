import { createSelector } from 'reselect'
import { INITIAL_STATE } from './Constants'

// export const selectWeatherCityId = (state) => {
//   return state.weather.form.selectedCity || ''
// }

const selectWeatherState = (state) => state.weather || INITIAL_STATE

const makeSelectWeatherState = () =>
  createSelector(
    selectWeatherState,
    (weatherState) => weatherState
  )

export default makeSelectWeatherState
export { selectWeatherState }
