import axios from 'axios'
import { Config } from 'App/Config'
// const {
//   WEATHER_SERVICE: {
//     API_URL: WEATHER_API_URL,
//     TEMPERATURE_UNIT: TMP_UNIT,
//     API_KEY: APPID,
//   },
// } = config

export const fetchWeatherApi = (cityId) => {
  return axios
    .request({
      url: Config.WEATHER_SERVICE.API_URL,
      method: 'GET',
      params: {
        id: cityId,
        units: Config.WEATHER_SERVICE.TEMPERATURE_UNIT,
        APPID: Config.WEATHER_SERVICE.API_KEY,
      },
    })
    .then((response) => response)
    .catch((err) => err)
}
