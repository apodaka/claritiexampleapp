/*

import cities from './city.list.json

TODO:
  Se deberán de gestionar en la base de datos
  las ciudades que se encuentran en el archivo "city.list.json"
  y obtener los datos de geolocalización los cuales serán utilizados
  para consultar la API de clima.

  (Traducir al inglés)
  
  Fuente: https://openweathermap.org
 */


/*
  REDUX ACTIONS
*/

export const REQUEST_WEATHER_CITY = 'App\Stores\Weather\REQUEST_WEATHER_CITY';
export const REQUEST_WEATHER_CITY_SUCCESS = 'App\Stores\Weather\REQUEST_WEATHER_CITY_SUCCESS';
export const REQUEST_WEATHER_CITY_FAILED = 'App\Stores\Weather\REQUEST_WEATHER_CITY_FAILED';
export const ADD_REQUEST_TO_STACK = 'App\Stores\Weather\ADD_REQUEST_TO_STACK';
export const SET_SELECTED_CITY = 'App\Stores\Weather\SET_SELECTED_CITY';

/*

GROUP ALL REDUX ACTIONS IN ONE CONSTANT, THIS IS FOR 
BIND DISPATCH TO ALL ACTIONS WITH bindActionsCreators FUNCTION

[example]

import { reduxActions } from 'App/Stores/Weather/Constants'
import { bindActionCreators } from 'redux'


...
...

const mapDispatchToProps = (dispatch) => {

  return {
    dispatch,
    storeActions: bindActionCreators(reduxActions, dispatch), 
  }
}

.. 
.. [in the component]

componentDidMount() {
  // this.props.dispatch(myAction()) // before bindActionsCreators
  
  // this.props.storeActions.myAction() // after use bindActionsCreators
}
*/


/* INITIAL DATA FOR REDUCER */
export const weatherCities = [
  {
    "id": 4012176,
    "name": "Culiacan",
    "country": "MX",
    "coord": {
      "lon": -107.389717,
      "lat": 24.79944
    }
  },
  {
    "id": 3997479,
    "name": "Los Mochis",
    "country": "MX",
    "coord": {
      "lon": -108.966667,
      "lat": 25.76667
    }
  },
  {
    "id": 3523412,
    "name": "Mazatlan",
    "country": "MX",
    "coord": {
      "lon": -99.48333,
      "lat": 17.450001
    }
  }
]

export const INITIAL_STATE = {
  datasources: {
    weather: null,
    cities: weatherCities,
  },
  form: {
    selectedCity: '',
  },
  requestsLogs: [],
}
