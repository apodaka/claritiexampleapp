import {
  REQUEST_WEATHER_CITY,
  REQUEST_WEATHER_CITY_SUCCESS,
  REQUEST_WEATHER_CITY_FAILED,
  ADD_REQUEST_TO_STACK,
  SET_SELECTED_CITY,
} from './Constants'

  export function requestWeatherCityAction() {
    return {
      type: REQUEST_WEATHER_CITY,
    }
  }

  export function requestWeatherCitySuccessAction(payload) {
    return {
      type: REQUEST_WEATHER_CITY_SUCCESS,
      payload,
    }
  }
  export function requestWeatherCityFailedAction(payload){
    return {
      type: REQUEST_WEATHER_CITY_FAILED,
      payload,
    }
  }
  export function setSelectedCityAction(value) {
    return {
      type: SET_SELECTED_CITY,
      value,
    }
  }

export default {
  requestWeatherCityAction,
  requestWeatherCitySuccessAction,
  requestWeatherCityFailedAction,
  setSelectedCityAction,
}
