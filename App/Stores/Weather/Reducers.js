import produce from 'immer';

import {
  INITIAL_STATE,
  REQUEST_WEATHER_CITY,
  REQUEST_WEATHER_CITY_SUCCESS,
  REQUEST_WEATHER_CITY_FAILED,
  SET_SELECTED_CITY,
} from './Constants';

const reducer = produce((draft, action) => {
  switch (action.type) {
    case REQUEST_WEATHER_CITY:
      break
    case REQUEST_WEATHER_CITY_SUCCESS:
      draft.datasources.weather = action.payload
      break
    case REQUEST_WEATHER_CITY_FAILED: {
      draft.requestsLogs.push(action.payload)
    }
      break;
    case SET_SELECTED_CITY: {
      draft.form.selectedCity = action.value
    }
  }
}, INITIAL_STATE);

export default reducer;
