import { StyleSheet } from 'react-native'
import Colors from 'App/Theme/Colors'
import ApplicationStyles from 'App/Theme/ApplicationStyles'
import { spacing, fontSizes } from 'App/Theme/Metrics'

export default StyleSheet.create({
  activityContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    ...ApplicationStyles.screen.container,
    flex: 1,
  },
  pickerContainer: {
    alignItems: 'stretch',
    margin: spacing(4),
  },
  rowContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    flexDirection: 'row',
    marginBottom: spacing(1),
    marginTop: spacing(2),
  },
  weatherIcon: {
    width: spacing(10),
    alignItems: 'center',
  },
  iconText: {
    ...fontSizes.normal,
  },
  link: {
    ...ApplicationStyles.link,
  },
})
