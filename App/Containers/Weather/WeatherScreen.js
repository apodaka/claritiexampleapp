import React, { useState, useEffect } from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Text, View, ActivityIndicator, Image, Picker, Linking } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { createStructuredSelector } from 'reselect'
import makeSelectWeatherState from 'App/Stores/Weather/Selectors'
import Images from 'App/Theme/Images'
import * as reduxActions from 'App/Stores/Weather/Actions'
import Style from './WeatherScreenStyles'
import { border } from 'App/Theme/Helpers'
import { spacing, fontSizes } from 'App/Theme/Metrics'
import isNull from 'lodash/isNull'
import isNumber from 'lodash/isNumber'
import get from 'lodash/get'

function useWeatherState(props) {
  const {
    weatherActions: { setSelectedCityAction },
  } = props
  const [initialized, setInitialized] = useState(false) // initial value
  const [isLoadingScreen, setLoadingScreen] = useState(true) // initial value
  // custom handlers
  const onChangePicker = (value, index) => {
    console.log('onChangePicker value', value)
    setSelectedCityAction(value)
  }
  return {
    state: {
      initialized,
      isLoadingScreen,
    },
    handlers: {
      onChangePicker,
      setInitialized,
      setLoadingScreen,
    },
  }
}

function WeatherScreen(props) {
  const {
    weather: {
      datasources: { cities, weather },
      form: { selectedCity },
    },
  } = props

  const {
    state: { initialized, isLoadingScreen },
    handlers: { onChangePicker, setInitialized, setLoadingScreen },
  } = useWeatherState(props)

  const temp = get(weather, 'main.temp', '')
  const temp_min = get(weather, 'main.temp_min', '')
  const temp_max = get(weather, 'main.temp_max', '')
  // lazy load screen loading
  console.log('temp_min in WeatherScreen', temp_min)
  console.log('temp_max in WeatherScreen', temp_max)
  // TODO: Remove this after the screen
  useEffect(() => {
    setTimeout(() => {
      setInitialized(true)
      setLoadingScreen(false)
    }, 1000)
  }, [initialized])

  // useEffect(() => {
  //   if (!isNull(weather)) {
  //     console.log('weather in useEffect', weather)
  //     // ntemp = get(weather, 'main.temp', null)
  //     temp_min = get(weather, 'main.temp_min', null)
  //     temp_max = get(weather, 'main.temp_max', null)
  //   }
  // }, [weather])

  return !initialized ? (
    <View style={Style.activityContainer}>
      <Text style={{ ...fontSizes.big }}>
        Cargando
      </Text>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  ) : (
    <View style={Style.container}>
      <View style={Style.rowContainer}>
        <Image source={Images.claritiLogo} />
        <View style={Style.weatherIcon}>
          <Text
            style={{
              ...fontSizes.normal,
            }}
          >
            Weather
          </Text>
          <FontAwesome5 name={'cloud-sun-rain'} solid color="#64b5f6" size={40} />
        </View>
      </View>
      <View style={Style.pickerContainer}>
        <Text
          style={{
            ...fontSizes.stretchLabel,
          }}
        >
          <Text>Muestras de ciudades obtenidas en </Text>
          <Text
            style={Style.link}
            onPress={() => Linking.openURL('https://openweathermap.org/current')}
          >
            openweathermap
          </Text>
        </Text>
        <Picker selectedValue={selectedCity} onValueChange={onChangePicker} mode="dropdown">
          <Picker.Item label="Seleccione una ciudad" value="" />
          {cities.map(({ id, name }) => (
            <Picker.Item key={id} label={name} value={id} />
          ))}
        </Picker>
        {isNumber(temp_min) > 0 && isNumber(temp_max) > 0 && (
          <View style={{
            flexDirection: 'row',
          }}>
            <View
            style={{
              flex: 1,
              marginTop: spacing(2),
              alignItems: 'center',
              justifyContent: 'center',
              width: spacing(10)
            }}
          >
            <Text>Temperatura máxima</Text>
            <FontAwesome5  name={'temperature-high'} solid color="#c62828" size={60} />
            <Text style={{ ...fontSizes.stretchLabel }}>{`${temp_max} °C`}</Text>
          </View>

          <View
            style={{
              flex: 1,
              marginTop: spacing(2),
              alignItems: 'center',
              justifyContent: 'center',
              width: spacing(10)
            }}
          >
            <Text>Temperatura mínima</Text>
            <FontAwesome5  name={'temperature-low'} solid color="#64b5f6" size={60} />
            <Text style={{ ...fontSizes.stretchLabel }}>{`${temp_min} °C`}</Text>
          </View>
          </View>
        )}
      </View>
    </View>
  )
}

// class ExampleScreen extends React.Component {
//   componentDidMount() {
//     this._fetchUser()
//   }

//   render() {
//     return (
//       <View style={Style.container}>
//         {this.props.userIsLoading ? (
//           <ActivityIndicator size="large" color="#0000ff" />
//         ) : (
//           <View>
//             <View style={Style.logoContainer}>
//               <Image style={Style.logo} source={Images.logo} resizeMode={'contain'} />
//             </View>
//             <Text style={Style.text}>To get started, edit App.js</Text>
//             <Text style={Style.instructions}>{instructions}</Text>
//             {this.props.userErrorMessage ? (
//               <Text style={Style.error}>{this.props.userErrorMessage}</Text>
//             ) : (
//               <View>
//                 <Text style={Style.result}>
//                   {"I'm a fake user, my name is "}
//                   {this.props.user.name}
//                 </Text>
//                 <Text style={Style.result}>
//                   {this.props.liveInEurope ? 'I live in Europe !' : "I don't live in Europe."}
//                 </Text>
//               </View>
//             )}
//             <Button onPress={() => this._fetchUser()} title="Refresh" />
//           </View>
//         )}
//       </View>
//     )
//   }

//   _fetchUser() {
//     this.props.fetchUser()
//   }
// }

WeatherScreen.propTypes = {
  // user: PropTypes.object,
  // userIsLoading: PropTypes.bool,
  // userErrorMessage: PropTypes.string,
  // fetchUser: PropTypes.func,
  // liveInEurope: PropTypes.bool,
  weather: PropTypes.object,
  weatherActions: PropTypes.object,
}

// const mapStateToProps = (state) => ({
//   user: state.example.user,
//   userIsLoading: state.example.userIsLoading,
//   userErrorMessage: state.example.userErrorMessage,
//   liveInEurope: liveInEurope(state),
// })

const mapStateToProps = createStructuredSelector({
  weather: makeSelectWeatherState(),
})

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  weatherActions: bindActionCreators(reduxActions, dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WeatherScreen)
