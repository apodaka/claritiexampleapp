import { put, call, select } from 'redux-saga/effects'
import ExampleActions from 'App/Stores/Example/Actions'
import { userService } from 'App/Services/UserService'
import get from 'lodash/get'

import { selectWeatherCityId } from 'App/Stores/Weather/Selectors'
import isNumber from 'lodash/isNumber'
import isEmpty from 'lodash/isEmpty'
import { fetchWeatherApi } from 'App/Stores/Weather/Api'
import { requestWeatherCitySuccessAction } from 'App/Stores/Weather/Actions'
/**
 * A saga can contain multiple functions.
 *
 * This example saga contains only one to fetch fake user informations.
 * Feel free to remove it.
 */

export function* fetchWeather(action) {
  const { value: cityId } = action
  console.log('cityId', cityId)
  console.log('typeof(cityId)', typeof cityId)
  if (isNumber(cityId)) {
    const response = yield call(fetchWeatherApi, cityId)
    console.log('fetchWeather response', response)
    if (response.data && Object.keys(response.data).length > 0) {
      yield put(requestWeatherCitySuccessAction(response.data))
    }
  } else {
    console.log('Nunca entró a la condición')
  }
}

// export function* fetchUser() {
//   // Dispatch a redux action using `put()`
//   // @see https://redux-saga.js.org/docs/basics/DispatchingActions.html
//   yield put(ExampleActions.fetchUserLoading())

//   // Fetch user informations from an API
//   const user = yield call(userService.fetchUser)
//   if (user) {
//     yield put(ExampleActions.fetchUserSuccess(user))
//   } else {
//     yield put(
//       ExampleActions.fetchUserFailure('There was an error while fetching user informations.')
//     )
//   }
// }
