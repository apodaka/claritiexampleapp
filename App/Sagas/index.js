import { takeLatest, all } from 'redux-saga/effects'
import { ExampleTypes } from 'App/Stores/Example/Actions'
import { StartupTypes } from 'App/Stores/Startup/Actions'
import { fetchWeather } from './WeatherSaga'
import { fetchUser } from './ExampleSaga'
import { startup } from './StartupSaga'

import { SET_SELECTED_CITY } from 'App/Stores/Weather/Constants'

export default function* root() {
  yield all([
    /**
     * @see https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
     */
    // Run the startup saga when the application starts
    takeLatest(StartupTypes.STARTUP, startup),
    // takeLatest()
    // Call `fetchUser()` when a `FETCH_USER` action is triggered
    takeLatest(ExampleTypes.FETCH_USER, fetchUser),
    takeLatest(SET_SELECTED_CITY, fetchWeather),
  ])
}
