import { StyleSheet } from 'react-native'
import get from 'lodash/get'
// import chain from 'lodash/chain'
import isUndefined from 'lodash/isUndefined'
import isString from 'lodash/isString'
import isNumber from 'lodash/isNumber'
/*

  [A very usefull style function]

  Date: September, 15 at 2019.   
  -----------------------------------------------

  Use this in combination with styles object
  for fast render a painted border in Views or
  other component (except <Text> ), usefull for
  layout design.

  -- DEFAULT VALUES --

  {
		borderWidth: 1,
		borderColor: 'black',
	}

  * Provide a string in first OR second argument for handle
    borderColor style prop (borderWidth Allways
    will be 1).

  * Provide a number in first OR second argument for handle
    borderWidth style prop (Allways will paint black
    color if only provide first argument as a number).

  * If no arguments were specified, will paint all borders
    in black using this style prop by default:

    {
      borderWidth: 1,
      borderColor: 'black',
    }

  -- how to use --

  <View style={{ ...border() }} />
  <View style={{ ...border(3) }} />
  <View style={{ ...border('red') }} />
  <View style={{ ...border('blue', 2) }} />
  // return 
  // {
  //   borderWidth: 2,
  //   borderColor: 'blue',
  // }
  <View style={{ ...border(4, 'green') }} />
  // return 
  // {
  //   borderWidth: 4,
  //   borderColor: 'green',
  // }

  StyleSheet.create({
    container: {
      ...border()
    }
  })
  
  //  No more.

  -----------------------
  Author: Erubiel Apodaca

*/

export function border(...args) {
  const [arg1, arg2] = args
  const styleRule = {
    borderWidth: 1,
    borderColor: 'black',
  }
  if (isUndefined(arg1)) {
    return styleRule
  }
  if (isString(arg1) && isUndefined(arg2)) {
    styleRule.borderColor = arg1
    return styleRule
  }
  if (isNumber(arg1) && isUndefined(arg2)) {
    styleRule.borderWidth = arg1
    return styleRule
  }
  if (isString(arg1) && isNumber(arg2)) {
    styleRule.borderWidth = arg2
    styleRule.borderColor = arg1
    return styleRule
  }
  if (isString(arg2) && isNumber(arg1)) {
    styleRule.borderWidth = arg1
    styleRule.borderColor = arg2
    return styleRule
  }
  return styleRule
}

export default StyleSheet.create({
  fill: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fillCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  rowReverse: {
    flexDirection: 'row-reverse',
  },
  column: {
    flexDirection: 'column',
  },
  columnReverse: {
    flexDirection: 'column-reverse',
  },
  mainStart: {
    justifyContent: 'flex-start',
  },
  mainCenter: {
    justifyContent: 'center',
  },
  mainEnd: {
    justifyContent: 'flex-end',
  },
  mainSpaceBetween: {
    justifyContent: 'space-between',
  },
  mainSpaceAround: {
    justifyContent: 'space-around',
  },
  crossStart: {
    alignItems: 'flex-start',
  },
  crossCenter: {
    alignItems: 'center',
  },
  crossEnd: {
    alignItems: 'flex-end',
  },
  crossStretch: {
    alignItems: 'stretch',
  },
  selfStretch: {
    alignSelf: 'stretch',
  },
  rowMain: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rowCross: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colMain: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  colCross: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  colCenter: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fillRow: {
    flex: 1,
    flexDirection: 'row',
  },
  fillRowReverse: {
    flex: 1,
    flexDirection: 'row-reverse',
  },
  fillRowMain: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fillRowCross: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  fillRowCenter: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fillCol: {
    flex: 1,
    flexDirection: 'column',
  },
  fillColReverse: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  fillColMain: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  fillColCross: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  fillColCenter: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textCenter: {
    textAlign: 'center',
  },
  textJustify: {
    textAlign: 'justify',
  },
  textLeft: {
    textAlign: 'left',
  },
  textRight: {
    textAlign: 'right',
  },
  backgroundReset: {
    backgroundColor: 'transparent',
  },
  fullWidth: {
    width: '100%',
  },
})
