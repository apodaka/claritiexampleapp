/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  logo: require('App/Assets/Images/TOM.png'),
  claritiLogo: require('App/Assets/Images/logo_clariti.png'),
}
