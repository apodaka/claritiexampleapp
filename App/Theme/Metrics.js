/**
 * values based on material design specifications
 * https://material.io/design/layout/understanding-layout.html#usage 
 */
export function spacing(n = 1, b = 8) {
  let _n = Number.isInteger(n) ? n : 1
  let _b = Number.isInteger(b) ? b : 8
  return _n * _b
}

export const inputHeight = {
  normal: {
    height: spacing(2),
  },
};

export const fontSizes = {
  small: {
    fontSize: spacing(),
  },
  normal: {
    fontSize: spacing(2),
  },
  stretchLabel: {
    fontSize: spacing(3),
  },
  big: {
    fontSize: spacing(4),
  },
}

export default {
  spacing,
  inputHeight,
}
